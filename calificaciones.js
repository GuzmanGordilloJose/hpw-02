var tabla_calificaciones=document.getElementById('tabla_calificaciones');
var cuerpo_tabla =tabla_calificaciones.children[1];
var aux=0;
var clave="";
var materia="";
var lista_calificacion_alta = document.getElementById('lista_calificacion_alta');
var lista_calificacion_baja = document.getElementById('lista_calificacion_baja');
var lista_promedio =document.getElementById('lista_promedio');
var tabla_calificaciones=document.getElementById('tabla_calificaciones');
var cuerpo_tabla =tabla_calificaciones.children[1];
var total=0;

function calcularPromedio(){
    for (var i=0; i<cuerpo_tabla.children.length; i++){
        total += Number(cuerpo_tabla.children[i].children[2].textContent);
    }
    var promedio=total  / cuerpo_tabla.children.length;
    lista_promedio.textContent=("Promedio: "+promedio);
}
    
function calcularCalificacionMasAlta(){
    for(var i = 0; i < cuerpo_tabla.children.length; i++){
        if(Number(cuerpo_tabla.children[i].children[2].textContent)>aux){
            clave   = String(cuerpo_tabla.children[i].children[0].textContent);
            materia = String(cuerpo_tabla.children[i].children[1].textContent);
            aux     = Number(cuerpo_tabla.children[i].children[2].textContent);
        }
    }
    lista_calificacion_alta.textContent=("Materia con mayor calificacion: "+clave+" "+materia+" "+aux);
}

function calcularCalificacionMasBaja(){
    for(var i = 0; i < cuerpo_tabla.children.length; i++){
        if(Number(cuerpo_tabla.children[i].children[2].textContent)<aux){
            clave   = String(cuerpo_tabla.children[i].children[0].textContent);
            materia = String(cuerpo_tabla.children[i].children[1].textContent);
            aux     = Number(cuerpo_tabla.children[i].children[2].textContent);
        }
    }
    lista_calificacion_baja.textContent=("Materia con menor calificacion: "+clave+" "+materia+" "+aux);
}

calcularPromedio();
calcularCalificacionMasAlta();
calcularCalificacionMasBaja();
